---
title:   Welcome to the Debian Kotlin GSoC blog
author:  Saif Abdul Cassim
---

I’ll be using this blog to track progress on packaging Kotlin and report on what I am doing during the GSoC period. So let me go on a head and start with the current progress in packaging Kotlin.

## Packaging Kotlin
### Kotlin 1.1.1
During GSoC 2018 I tried to package kotlin-1.1.1 because it was built with ant and looked easy, but as it turned out it had its shortcomings. For starters, since it was very old, it missed a lot of the dependencies that were supposed to be up and hosted by Jetbrains. I had to patch up these dependencies with the closest matching versions and got things building. Recently it ran into a massive hiccup and the problem just couldn't be solved as I couldn't figure which dependency was causing it. I opened an [issue](https://youtrack.jetbrains.com/issue/KT-31041) with Jetbrains and the conclusion I arrived at was that Kotlin 1.1.1 is too old and not supported anymore.

So following the advice of the Kotlin devs I have proceeded to take on the task of packaging Kotlin 1.3.30 (a couple months old version) into Debian.

### Kotlin 1.3.30
This is my Google Summer of Code 2019 project.

This is the [source](https://github.com/JetBrains/kotlin/tree/1.3.30) for Kotlin 1.3.30. Unlike Kotlin 1.1.1, Kotlin 1.3.30 had everything it needed to build successfully already hosted. So I went on ahead and split the work into 3 subtasks as follows:

1. Convert all gradle build scripts from kts to groovy so that we dont have
   to depend on kotlin-gradle-dsl.
2. Downgrade the build from gradle 5.1 to gradle 4.4.1 which is the version
   available in Debian Sid.
3. Package the dependencies of Kotlin 1.3.30 so that it builds fine using
   packages from the Debian Sid software environment alone.

A major blocker for this would in 3, when we are packaging the dependencies of
Kotlin 1.3.30. We would have to package IDEA and this will be a huge task.
Also yesterday I noted that Jetbrains has stopped hosting Kotlin [1.3.30-eap-28](https://plugins.gradle.org/m2/org/jetbrains/kotlin/kotlin-gradle-plugin/)
gradle plugin which is the bootstrap for Kotlin 1.3.30. I have gotten things to
build with 1.3.30 gradle plugin and have opened an [issue](https://youtrack.jetbrains.com/issue/KT-31642) with Jetbrains asking
them to prolong hosting the dependecies for Kotlin 1.3.30.

You can see my work on kotlin 1.3.30 [here](https://salsa.debian.org/m36-guest/kotlin-1.3.30) and I will be posting here in this
blog once every week with the major updates. You can find me as `m36[m]` or `m36`
in `#debian-java` or `#debian-mobile` channels hosted at OFTC.
