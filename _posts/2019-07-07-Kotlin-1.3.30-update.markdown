---
title:  Packaging Dependencies Part 1; and plan on how to
author:  Saif Abdul Cassim
---

###Finished downgrading the project to be buildable by gradle 4.4.1

I have finished downgrading the project to be buildable using gradle 4.4.1. The project
still needed a part of gradle 4.8 that I have successfully patched into sid gradle.
[here](https://salsa.debian.org/java-team/gradle/commit/f6623d76ade6f515423669f55d0b3508f9e99c6b) is the link to the changes that I have made.

Now we are officially done with making the project build with our gradle; so we can now go on ahead
and finally start mapping out and packaging the dependencies.

###Packaging dependencies for Kotlin-1.3.30

I split this task into two sub tasks that can be done independently. The 2 subtasks are as follows:

  * part 1: make the entire project build successfully without :buildSrc:prepare-deps:intellij-sdk:build
    * part1.1:package these dependencies
  * part 2: package the dependencies in :buildSrc:prepare-deps:intellij-sdk:build; i.e. try to recreate whatever is in it.

The task has been split into this exact model because [this](https://salsa.debian.org/m36-guest/kotlin-1.3.30-temp-requirements) folder has a variety of JARs that the project uses and we’ll have to
minimize it and package the needed jars from it. Also the project uses other plugins and JARs other than this one main folder which we can simultaneously
map and package out.

I now have successfully mapped out the dependencies need by part 1: all that remains is to package em.
I have copied the dependencies from the original cache(one created when I build the project using `./gradlew -Pteamcity=true dist`) to /usr/share/maven-repo
so some of these dependencies still need to have their dependencies clearly defined as in which of their dependencies we can omit and which we need. I have marked such
dependencies with a `*`. So here are the dependencies:

 * jengeleman:shadow:4.0.3 → https://github.com/johnrengelman/shadow (DONE: https://salsa.debian.org/m36-guest/jengelman-shadow)
 * trove4j 1.x → https://github.com/JetBrains/intellij-deps-trove4j (DONE: https://salsa.debian.org/java-team/libtrove-intellij-java)
 * proguard:6.0.3 in jdk8 (DONE: released as libproguard-java 6.0.3-2)
 * io.javaslang:2.0.6 → https://github.com/vavr-io/vavr/tree/javaslang-v2.0.6 (DONE:https://salsa.debian.org/m36-guest/javaslang)
 * jline 3.0.3  → https://github.com/jline/jline3/tree/jline-3.3.1 (DONE: https://salsa.debian.org/m36-guest/jline3)
 * protobuf-2.6.1 in jdk8 (DONE: https://salsa.debian.org/java-team/protobuf-2)
 * com.jcabi:jcabi-aether:1.0  → the file that requires this is commented out; can be seen [here](https://salsa.debian.org/m36-guest/kotlin-1.3.30/commit/cb298ba550ca9f727ff66e4ffca0cb73e9ee03f1) and [here](https://salsa.debian.org/m36-guest/kotlin-1.3.30/commit/1503110b35bd701388a30cd7358bca780b2ab2ce)
 * org.sonatype.aether:aether-api:1.13.1 → the file that requires this is commented out; can be seen [here](https://salsa.debian.org/m36-guest/kotlin-1.3.30/commit/cb298ba550ca9f727ff66e4ffca0cb73e9ee03f1) and [here](https://salsa.debian.org/m36-guest/kotlin-1.3.30/commit/1503110b35bd701388a30cd7358bca780b2ab2ce)

NOTE: Please note that I might have missed out some; I’ll add them to the list once I get them mapped out proper!

So if any of you kind souls wanna help me out please kindly take on any of these and package them.

NOTE — ping me if you want to build kotlin in your system and are stuck!

[Here](https://salsa.debian.org/m36-guest/kotlin-1.3.30) is a link to the work I have done so
far. You can find me as `m36` or `m36[m]` on `#debian-mobile` and `#debian-java` in OFTC.

I’ll try to maintain this blog and post the major updates weekly.
